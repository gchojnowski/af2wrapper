# Copyright (C) 2022 Grzegorz Chojnowski
# EMBL-Hamburg
from setuptools import setup, find_packages
import setuptools
import subprocess
import distutils.log
import distutils.command.build
from distutils.command.install import install as DistutilsInstall


class SconsCommand(setuptools.Command):

  def run(self):
    """Run command."""
    command = ['cd lib; scons']
    self.announce('Compiling smatch.so', level=distutils.log.INFO)
    subprocess.check_call(command)

class MyInstall(DistutilsInstall):
    def run(self):
        subprocess.check_call('scons', cwd='./lib')
        DistutilsInstall.run(self)

def get_git_describe(abbrev=7):
    import af2start.version
    return af2start.version.__version__


    try:
        p = subprocess.Popen(['git', 'describe', '--abbrev=%d' % abbrev],
                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.stderr.close()
        line = p.stdout.readlines()[0]
        version = line.strip().decode('ascii')
        # remove pep440-incompatible sha tag
        if len(version.split('-'))>1:
            version = ".".join(re.split(r'[-.]',version)[:-1])

        with open("af2start/version.py", "w") as ofile:
            ofile.write("__version__=\"%s\""%version)
        return version
    except:
        return None

_ext_modules=[]
_package_data={'af2start':['examples/*'], 'af2plots':['examples/PIAQ_test_af2mmer_dimer/*', 'examples/PIAQ_test_af2mmer_dimer/input/*', 'examples/PIAQ_test_af2mmer_dimer/input/msas/A/*']}
_cmdclass={}

setup(
    cmdclass={},
    name="af2wrapper",
    version=get_git_describe(),
    packages=['af2start', 'af2plots', 'af2plots.af2plots'],
    package_data=_package_data,
    url="https://git.embl.de/gchojnowski/af2wrapper",
    license="BSD",
    author="Grzegorz Chojnowski",
    author_email="gchojnowski@embl-hamburg.de",
    description="AlphaFold2/ColabFold wrapper",
    zip_safe=False,
    entry_points={
          "console_scripts": [
            "af2start = af2start.__main__:main",
            ],
    }
)


