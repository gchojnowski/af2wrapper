# af2wrapper: a tool for handling AlphaFold2/ColabFold jobs


For handling (minimal) dependencies you can use **conda** or **miniconda** environments. Detailed installation instructions for each of them can be found [here](https://docs.conda.io/en/latest/index.html) and [here](https://docs.conda.io/en/latest/miniconda.html). After installing conda (if needed) create and activate an enironment:

```
conda create -n af2wrapper
conda activate af2wrapper
```

alternatively, you can use miniconda. It doesn't require any changes to you system and it's fully contained in a local directory. To install it download an installer from [here](https://docs.conda.io/en/latest/miniconda.html) and run the following (linux example):

```
bash Miniconda3-latest-Linux-x86_64.sh -b -p miniconda_af2wrapper
. miniconda_af2wrapper/bin/activate
```

Now, you can proceed with the installation instructions below


```
conda install -c conda-forge matplotlib
```

# Installation

First, pull the repository

```
git clone --recursive git@git.embl.de:gchojnowski/af2wrapper.git
```

and install it with

```
cd af2wrapper
python setup.py install
```

# Site-specific setup

First, you need to cerate a site-specific configuration file. To create a template named *test.conf* run

```
af2start --conf test.conf
```

and adjust the parameters for your local AlphaFold2/ColabFold installation:


```
{
    "path_to_run_alphafold_py": "None",
    "path_to_run_docker": "None",
    "path_to_colabfold_batch": "None",
    "max_template_date": "2020-05-14",
    "data_dir": "/DB",
    "db_preset": "full_dbs",
    "pdb70_database_path": "/DB/pdb70/pdb70",
    "uniref90_database_path": "/DB/uniref90/uniref90.fasta",
    "mgnify_database_path": "/DB/mgnify/mgy_clusters_2018_12.fa",
    "template_mmcif_dir": "/DB/pdb_mmcif/mmcif_files/",
    "obsolete_pdbs_path": "/DB/pdb_mmcif/obsolete.dat",
    "bfd_database_path": "/DB/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt",
    "uniclust30_database_path": "/DB/uniclust30/uniclust30_2018_08/uniclust30_2018_08",
    "pdb_seqres_database_path": "/DB/pdb_seqres/pdb_seqres.txt",
    "uniprot_database_path": "/DB/uniprot/uniprot.fasta",
    "use_gpu_relax": "1",
    "colabfold_use_cpu": "0",
    "hhblits_binary_path": "hhblits",
    "hhsearch_binary_path": "hhsearch",
    "hmmbuild_binary_path": "hmmbuild",
    "hmmsearch_binary_path": "hmmsearch",
    "jackhmmer_binary_path": "jackhmmer",
    "kalign_binary_path": "kalign",
    "run_this_cmd_before_af2": "load af2 module",
    "run_this_cmd_before_cf": "load ColabFold module"
}
```

*run_this_cmd_before_af2* and *run_this_cmd_before_af2* must contain a single line with semicolon-separated commands (or system variable definitions) you need to run on your system before running AF2/CF. Leave it blank ("") if no additional configuration is needed. The configuration file must be later defined with a system variable *ALPHAFOLD_CFG*

## Docker-based AlphaFold2 installation

If you use docker-based installation of AlphaFold2 you need to define keyword *path_to_run_docker* e.g. as *python3 /opt/docker/run_docker.py* and only the following keywords in configuration file:

 - max_template_date
 - db_preset: *full_dbs* or *reduced_dbs*
 - data_dir: Path to directory of supporting data
 - use_gpu_relax: *1* if you intent to use GPU at all (not only for model relaxation), otherwise set to *0*


# Important: required binaries

By default the following executables must be available in *PATH* wherever you intent to run the script: *hhblits, hhsearch, hmmbuild, hmmsearch, jackhmmer, and kalign*. You can adjust the default configuration to use specific executables. ColabFold backend (*--colabfold*) uses remote MMseqs2 API and doesn't require local sequence databases or search tools.

# Test


After adjusting the configuration file (eg *test.conf*) you can test the installation with:


```
ALPHAFOLD_CFG=test.conf af2start --seqin examples/PIAQ_test_af2mmer_dimer/input.fasta --out test_results --test
```

This should print a runnable (copy-paste) AF2 command and generate an HTML-report *test_results/report.html* for a copy of an attached example AF2 prediction.

# Basic usage

Most basic usage: 

```
ALPHAFOLD_CFG=test.conf af2start --seqin input.fasta  --out output
```

A prediction with amber relaxation enabled:

```
ALPHAFOLD_CFG=test.conf af2start --seqin input.fasta  --out output --relax
```



